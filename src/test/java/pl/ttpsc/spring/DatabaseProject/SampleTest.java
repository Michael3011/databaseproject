package pl.ttpsc.spring.DatabaseProject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
public class SampleTest {
    @Test
    public void shouldAddNumbers() {
        assertThat(2+2)
                .isEqualTo(4);
    }
}
