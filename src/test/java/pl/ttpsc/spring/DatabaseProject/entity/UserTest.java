package pl.ttpsc.spring.DatabaseProject.entity;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserTest {
    private User user;

    @Before
    public void createUser() {
        user = new User();
    }

    @Test
    public void shouldAuthorities() {
        //given
        user.setRole("ADMIN");
        //when
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        GrantedAuthority grantedAuthority = authorities.iterator().next();
        String grantedAuthorityName = grantedAuthority.getAuthority();
        //then
        assertThat(grantedAuthorityName)
                .isEqualTo("ROLE_ADMIN");
    }

    @Test
    public void shouldGetAuthoritiesReturnsNotNull() {
        //when
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        //then
        assertThat(authorities)
                .isNotNull();
    }

    @Test
    public void shouldGetAuthoritiesBeEmptyCollection() {
        //when
            Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
            Collection<? extends GrantedAuthority> emptyCollection = new ArrayList<>();
            //then
            assertThat(authorities)
                    .isEqualTo(emptyCollection);
    }

    @Test
    public void ShouldGetAuthoritiesBeNotEmpty() {
        //Given
        user.setRole("USER");
        //when
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        int authoritiesLength = authorities.size();
        //then
        assertThat(authoritiesLength)
                .isEqualTo(1);
    }
}