package pl.ttpsc.spring.DatabaseProject.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import pl.ttpsc.spring.DatabaseProject.entity.User;
import pl.ttpsc.spring.DatabaseProject.repository.UserRepository;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.assertj.core.api.Java6Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class DatabaseUserDetailsServiceTest {

    @Mock
    private UserRepository repository;

    @Spy
    private  UserRepository repositorySpy;

    @InjectMocks
    private DatabaseUserDetailsService userDetailsService;

    @Test
    public void shouldLoadUserByUserName() {
        //given
        when(repository.findByUsername(anyString())).thenReturn(new User("mocked"));
        //when
        User user = repository.findByUsername("mocked");
        //then
        assertThat(user.getName())
            .isEqualTo("mocked");
    }

    @Test
    public void shouldThrowUsernameNotFoundException() {
        //given
        when(repository.findByUsername(anyString())).thenReturn(null);
        //when
        Throwable thrown = catchThrowable(() -> userDetailsService.loadUserByUsername("mocked"));
        //then
        assertThat(thrown)
                .isInstanceOf(UsernameNotFoundException.class);
    }

    @Test
    public void shouldCallRepositoryOnce() {
        //given
        when(repository.findByUsername(anyString())).thenReturn(new User("mocked"));
        //when
        UserDetails userDetails = userDetailsService.loadUserByUsername("mocked");
        //then
        verify(repository, times(1)).findByUsername(anyString());
    }
}