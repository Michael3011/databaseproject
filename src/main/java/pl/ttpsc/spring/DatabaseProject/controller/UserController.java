package pl.ttpsc.spring.DatabaseProject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ttpsc.spring.DatabaseProject.entity.Project;
import pl.ttpsc.spring.DatabaseProject.entity.User;
import pl.ttpsc.spring.DatabaseProject.repository.ProjectRepository;
import pl.ttpsc.spring.DatabaseProject.repository.UserRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private ProjectRepository projects;
    @Autowired
    private UserRepository users;

    private static final Logger LOGGER = LoggerFactory.getLogger("application");

    @GetMapping("/users")
    public List<User> showUsers() {
        LOGGER.info("Lit of users displayed");
        return users.findAll();
    }

    @PostMapping("/users")
    @Transactional
    public void addUser(@RequestBody @Valid User user) {
        Project project = new Project(user.getName()+"'s main project", "Auto generated project for: " + user.getName(), user);
        user.setProjects(new ArrayList<Project>());
        user.getProjects().add(project);
        users.save(user);
        projects.save(project);
        LOGGER.info("New user: " + user.getCreated());
    }

}
