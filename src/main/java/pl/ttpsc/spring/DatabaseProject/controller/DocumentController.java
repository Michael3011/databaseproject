package pl.ttpsc.spring.DatabaseProject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ttpsc.spring.DatabaseProject.entity.Document;
import pl.ttpsc.spring.DatabaseProject.entity.Project;
import pl.ttpsc.spring.DatabaseProject.projections.IdTitleOnly;
import pl.ttpsc.spring.DatabaseProject.repository.DocumentRepository;
import pl.ttpsc.spring.DatabaseProject.repository.ProjectRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DocumentController {
    @Autowired
    private DocumentRepository documents;
    @Autowired
    private ProjectRepository projects;

    private static final Logger LOGGER = LoggerFactory.getLogger("application");

    @GetMapping("/projects/{projectId}/documents")
    public List<IdTitleOnly> showDocuments(@PathVariable("projectId") int projectId) {
        LOGGER.info("Lit of documents for project: [" + projectId +"] displayed");
        return documents.findByProject(projects.findById(projectId));

    }

    @GetMapping("/projects/{projectId}/documents/{documentId}")
    public Document showOneDocument(@PathVariable("projectId") int projectId, @PathVariable("documentId") int documentId) {
        Project project = projects.findById(projectId);
        LOGGER.info("Document with ID: [" + documentId + "] displayed");
        return documents.findByIdAndProject(documentId, project);
    }

    @PostMapping("/projects/{projectId}/documents")
    public void addDocument(@PathVariable("projectId") int projectId, @RequestBody @Valid Document document) {
        Project project = projects.findById(projectId);
        document.setProject(project);
        documents.save(document);
        LOGGER.info("New document added for project [" + projectId +"}: " + document.getCreated());
    }
}

