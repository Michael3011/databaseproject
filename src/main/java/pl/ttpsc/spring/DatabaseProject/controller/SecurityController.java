package pl.ttpsc.spring.DatabaseProject.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class SecurityController {
    @GetMapping("/security/principal")
    public String showCurrentUser(Principal principal) {
        return principal.getName();
    }

    @GetMapping("/security/auth")
    public Object showCurrentAuth(Authentication auth) {
        return auth.getDetails();
    }

    @GetMapping("/security")
    public String showTextMessage() {
        return "Message";
    }
}
