package pl.ttpsc.spring.DatabaseProject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import pl.ttpsc.spring.DatabaseProject.entity.ApiError;
import pl.ttpsc.spring.DatabaseProject.entity.Project;
import pl.ttpsc.spring.DatabaseProject.repository.ProjectRepository;

import javax.validation.Valid;

@RestController
public class ProjectController {
    @Autowired
    private ProjectRepository projects;

    private static final Logger LOGGER = LoggerFactory.getLogger("application");


    @GetMapping("/projects")
    public Iterable<Project> filterProjects(@RequestParam(value = "name", required = false) String name) {
        if(name == null) {
            LOGGER.info("Lit of projects displayed");
            return projects.findBy();
        }
        LOGGER.info("Lit of projects with name: [" + name + "] displayed");
        return projects.findByName(name);
    }

    @PostMapping("/projects")
    public void addProject(@RequestBody @Valid Project project) {
        projects.save(project);
        LOGGER.info("New project added:  " + project.getCreated());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiError handleException(MethodArgumentNotValidException e) {
        return new ApiError(e.getMessage());
    }
}
