package pl.ttpsc.spring.DatabaseProject.entity;


import javax.persistence.Embeddable;

@Embeddable
public class Status {
    private String comment;
    private String phase;

    public Status() { }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }
}

