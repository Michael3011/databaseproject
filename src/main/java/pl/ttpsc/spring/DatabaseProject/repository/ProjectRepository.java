package pl.ttpsc.spring.DatabaseProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import pl.ttpsc.spring.DatabaseProject.entity.Project;

import javax.annotation.security.DenyAll;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    Iterable<Project> findByName(String name);

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    Iterable<Project> findBy();

    @DenyAll
    void deleteAll();

    Project findById(int id);
}
