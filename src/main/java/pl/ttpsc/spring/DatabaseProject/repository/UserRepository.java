package pl.ttpsc.spring.DatabaseProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ttpsc.spring.DatabaseProject.entity.Project;
import pl.ttpsc.spring.DatabaseProject.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findById(int id);
    User findByUsername(String username);
}
