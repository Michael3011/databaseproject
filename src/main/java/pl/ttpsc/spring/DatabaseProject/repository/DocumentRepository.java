package pl.ttpsc.spring.DatabaseProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ttpsc.spring.DatabaseProject.entity.Document;
import pl.ttpsc.spring.DatabaseProject.entity.Project;
import pl.ttpsc.spring.DatabaseProject.projections.IdTitleOnly;

import javax.persistence.Id;
import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Integer> {
    List<IdTitleOnly> findByProject(Project project);
    Document findByIdAndProject(int id, Project project);
}
