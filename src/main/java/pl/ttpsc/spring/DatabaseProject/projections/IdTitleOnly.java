package pl.ttpsc.spring.DatabaseProject.projections;

public interface IdTitleOnly {
    String getId();
    String getTitle();
}
